m4_changequote(<|, |>)
#include <stdio.h>
#include <limits.h>
#include <ctype.h>
static const char bytes[] = {
	m4_esyscmd(xxd -i < THE_SOURCE), 0x0
};

/* If *p is digit, return it; return INT_MAX otherwise */
static int digit(const char *p)
{
	const char c = *p;
	if (isdigit(c)) {
		return c - '0';
	}
	return INT_MAX;
}

int
main(int argc, char **argv)
{
	const char *p = bytes;
	while (*p) {
		int ix;
		if (*p == '#' && ((ix = digit(p+1)) < argc)) {
			fputs(argv[ix], stdout);
			p += 2;
		} else {
			putchar(*p);
			p += 1;
		}
	}
	return 0;
}
// vim: ft=c
