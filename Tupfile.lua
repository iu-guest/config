uri  = {}
rule = {}
directory = {}
function uri.github(arg)
  local fmt = 'https://raw.githubusercontent.com/%s/%s/%s'
  return string.format(fmt, arg.repo, arg.ref or 'master', arg.path)
end

do
  files = {}
  files.relatives = {}
  files.inputs = {}

  function outfile(relative)
    local input = string.format('out/dest/%s', relative)
    table.insert(files.relatives, relative)
    table.insert(files.inputs, input)
    return input
  end

  function rule.archive()
    local tgz = 'out/config.tar.gz'
    local cmd = [[
^ TAR config.tar.gz^ cd out/dest; tar -czf ../config.tar.gz %s ]]
    local argument = tostring(files.relatives)

    tup.rule(files.inputs, cmd:format(argument), tgz)
  end
end

function rule.copy(relative)
  local relative = string.sub(relative, 3)
  local source = string.format('src/copy/%s', relative)
  local destination = outfile(relative)
  local command = string.format('^ CP %s^ cp -d %%f %%o', relative)
  tup.rule(source, command, destination)
end

function rule.vimspell(input)
  local lang = tup.file(input)
  local path = string.format('.vim/spell/%s.utf-8.spl', lang)
  local command = string.format([[^ SPELL %s^
  printf "mkspell! %%o %%f\nexit" | vim -e ]], base)
  tup.rule(input, command, outfile(path))
end

function rule.deploy()
  local command = '^ DEPLOY^ cp -f %f ~/ && cd ~ && tar xf %b'
  tup.rule('out/config.tar.gz', command, {})
end

function rule.man(input)
  local ext  = tup.ext(input)
  local base = tup.base(input)
  local dest = string.format('.local/share/man/man%s/%s.%s.gz', ext, base, ext)
  tup.rule(input, '^ MAN %f^ gzip %f -c > %o', outfile(dest))
end

function rule.dircolors(dest, input)
  local cmd = '^ DIRCOLORS ^ TERM=linux dircolors --sh %s > %%o'
  tup.rule(input or {}, cmd:format(input or ""), outfile(dest))
end

function rule.ftplugin(input)
  local relative = input:gsub('^src/copy/', '')
  local output = outfile(relative)
  tup.rule(input, '^ FTPLUGIN %B^ ./aux/fix-ftplugin %f > %o', output)
end

function rule.m4(dest, input)
  local cmd = '^ M4 %b^ m4 -P %f > %o'
  tup.rule(string.format('src/m4/%s', input), cmd, outfile(dest))
end

function rule.snippet_txt(source)
  local base = tup.base(source)
  local src_c = string.format('out/snippet_txt/%s.c', base)
  local cmd_m4 = '^ SNIP %B^ m4 -P -DTHE_SOURCE=%f aux/mk-template.c.m4> %o'
  tup.rule(source, cmd_m4, src_c)
  tup.rule(src_c, '^ CC %B^ c99 -O3 %f -o %o', outfile('.vim/bin/' .. base))
end

function directory.within(list, callback)
  for dir in list do
    local pattern = string.format('src/copy/%s/*', dir)
    for _, path in ipairs(tup.glob(pattern)) do
      local relative = string.format('%s/%s', dir, tup.file(path))
      callback(relative)
    end
  end
end

tup.creategitignore()
directory.within(io.lines('src/copy.dirs'), rule.copy)

for _, input in ipairs(tup.glob('src/spell/*')) do
  rule.vimspell(input)
end

for _, input in ipairs(tup.glob('src/man/*')) do
  rule.man(input)
end

for _, input in ipairs(tup.glob('src/prog/*')) do
  local file = tup.file(input)
  local dest = string.format('.local/bin/%s', file)
  tup.rule(input, '^ INSTALL %B^ cp -f %f %o', outfile(dest))
end

do
  local names = {}
  for _, input in ipairs(tup.glob('src/snippet-txt/*.txt')) do
    rule.snippet_txt(input)
    table.insert(names, tup.base(input))
  end
  local command = '^ SNIPCMD^ ./aux/vim-snipcmd %s > %%o'
  local output = outfile('.vim/autogen/snippets.vim')
  tup.rule({}, command:format(tostring(names)), output)
end


do
  local pattern = 'src/copy/.vim/after/ftplugin/*.vim'
  for _, input in ipairs(tup.glob(pattern)) do
    rule.ftplugin(input)
  end
end

do
  local filename = 'volatile/dircolors.256dark'
  rule.dircolors('.local/share/posix/dircolors-256.sh', filename)
  rule.dircolors('.local/share/posix/dircolors-16.sh')
  rule.m4('.local/share/posix/color-list.sh', 'colorlist.m4')
end

tup.rule({}, './aux/myrepos.salsa < volatile/salsa.list > %o',
                 outfile('.config/myrepos/salsa.conf'))

rule.archive()

if tup.getconfig('DEPLOY') == 'y' then
  rule.deploy()
end
