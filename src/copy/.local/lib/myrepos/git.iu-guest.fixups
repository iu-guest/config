#!/bin/sh -eu

if ! ~/.local/lib/myrepos/check.private-key ; then
    printf 'Secret GnuPG key %s is not available.\n' "${KEY_GPG_MASTER}"
    printf 'Leaving repository with read-only access.\n'
    exit 0
fi

readonly name="${1}"

# Storing my GitLab token on iu-guest.info seems to complicate setup,
# so mirroring is performed on client side. At least, I automate it.

readonly gl_name=$(printf %s "${name}" | tr /. -)

git config --replace-all 'remote.origin.pushURL' "git@git.iu-guest.info:${name}"
git config --add 'remote.origin.pushURL' "git@gitlab.com:iu-guest/${gl_name}"

# Now, we make sure that GitLab mirror actually exists. If not, we have
# to perform several curl requests to API to create repository.
token=$(pass www/gitlab.com/iu-guest | awk '/token:/ { print $2 }')
gitlab () {
    local endpoint=$1; shift
    command curl --header "Private-Token: ${token}" --silent "$@" \
        "https://gitlab.com/api/v4/${endpoint}"
}

repo_id=$(gitlab "projects/iu-guest%2F${gl_name}" | jq .id)
if [ "${repo_id}" = null ] ; then # repository does not exist, create it
    : ${visibility:=public}
    gitlab projects --data "name=${gl_name}" \
                    --data "visibility=${visibility}" \
                    projects
fi
