tether() {
    sudo ip link set usb0 up
    sudo ip addr add 192.168.42.29/24 dev usb0
    sudo ip route add default via 192.168.42.129 dev usb0
    printf 'nameserver 192.168.42.129\n' |
        sudo tee /etc/resolv.conf >/dev/null
}
