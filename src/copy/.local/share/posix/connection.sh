__ssh_vps () {
    [ $# = 0 ] && set -- -D 1080 -l iu
    ssh -p 2223 "${VPS}" "$@"
}
alias ssh-vps='__ssh_vps'
