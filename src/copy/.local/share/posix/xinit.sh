which xinit >/dev/null || return

XInit() {
    export LC_ALL=C.UTF-8
    sudo xinit /bin/bash -c 'exec sudo -u iu ~iu/.xsession' \
        > /dev/null 2>/dev/null &
}
