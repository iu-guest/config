[ -x /usr/bin/mh/show ] || return

__newdraft () {
     /usr/bin/mh/"$@"
     $EDITOR "$(mhpath +drafts l)"
}
alias repl='__newdraft repl'
alias forw='__newdraft forw'
alias comp='__newdraft comp'
mhdraft () {
    [ $# = 0 ] && set -- l
    $EDITOR "$(mhpath +drafts "$@")"
}
[ "${BASH_VERSION}" ] && complete -F _nmh mhdraft
