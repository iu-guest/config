include dircolors-16
case "${TERM}" in
    *256*)
        include dircolors-256 ;;
esac
alias ls='ls --color'
