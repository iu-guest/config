export EMAIL='KAction@debian.org'
export DEBEMAIL='KAction@debian.org'
export DEBFULLNAME='Dmitry Bogatov'
export EDITOR='vi'

export KEY_GPG_MASTER='7214B3C8D5FF8F26E12593D52E20FEEE71FC7D81'
export KEY_GPG_SIGN='8671D5CC36ED747EE4B4A8F84812D8DEA82611E6'
export KEY_GPG_AUTH='3DCBBFB61F35293F3FF9ED5099CFF0FCE633C826'
export KEY_GPG_ENCR='A856CA76841C14506C00A72DA887F3D1A113FE37'
export BROWSER='w3m'
export SURFRAW_browser='w3m'
export LANG='C.UTF-8'
export SSH_AUTH_SOCK="${HOME}/.gnupg/S.gpg-agent.ssh"
export MANWIDTH='80'
export PATH="${HOME}/notes/bin:${HOME}/.local/bin:/usr/bin/mh:${PATH}"
export TTY="$(tty)"
export GPG_TTY="${TTY}"
export VPS='185.203.112.45'
export GDBHISTFILE="${HOME}/.cache/gdb/history"
### GuixSD-specific variables {{{
if [ -d "$HOME/.guix-profile/etc/ssl/certs" ] ; then
	SSL_CERT_DIR="$HOME/.guix-profile/etc/ssl/certs"
	SSL_CERT_FILE="$HOME/.guix-profile/etc/ssl/certs/ca-certificates.crt"
	GIT_SSL_CAINFO="$SSL_CERT_FILE"
fi
### }}}
