exec_dvtm () { exec dvtm -m '[' ; }
case "${TTY}" in
    # It disables klog messages being writter directly on terminal.
    # Such messages really confuses dvtm, and I can't really blame it.
    (/dev/tty*) sudo setterm --msg off ;;
esac
case "${TTY}" in
    (/dev/tty1) [ -n "${DVTM}" ] || exec_dvtm ;;
    (/dev/tty4) exec /bin/sh ;;
    (/dev/tty5) exec startx ;;
    (/dev/pts/*)
        if [ -z "${DVTM}" ] && [ -n "${DISPLAY}" ] ; then
            exec_dvtm
        fi
        ;;
esac
unset -f exec_dvtm
