hbuild() {
    stack install          \
        --fast             \
        -j4                \
        --haddock          \
        --test             \
        --haddock-deps     \
        --haddock-internal \
        --test
}

hpkg () {
    local pkg="${1:-base}"
    for index in \
        /usr/share/doc/ghc-doc/html/libraries/${pkg}-*/index.html \
        /usr/share/doc/libghc-${pkg}-doc/html/index.html
    do
        if [ -f "${index}" ] ; then
            "${BROWSER}" "${index}"
            return
        fi
    done
    "${BROWSER}" "https://stackage.org/lts-11.2/package/${pkg}"
}
