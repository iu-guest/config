include color
PS1='$(__prompt)'
__prompt () {
    local ERRNO=$?
    local gitroot display_root relrepo colorspec pretty_pwd
    set +u # makes some tests simplier
    [ "${SHLVL}" ] && color --yellow "($SHLVL)"
    color --xblack "${HOSTNAME}|"
    if gitroot="$(git rev-parse --show-toplevel 2> /dev/null)" ; then
        if [ "$(basename "$gitroot")" = debian ] ; then
            display_root=$(basename "$(dirname "$gitroot")")/debian
        else
            display_root=$(basename "$gitroot")
        fi
        relrepo=$(echo -n "${PWD}" | sed -e "s:$gitroot::")

        color --cyan { --yellow "${display_root}" --white "${relrepo}" --cyan }
    fi
    pretty_pwd=$(echo "$PWD" | sed -e 's#$#/#' -e "s#$HOME/#~/#" -e 's#//$#/#')
    colorspec='--xred'
    [ -w "${PWD}" ] && colorspec='--xblue'
    color "$colorspec" "$pretty_pwd" --xgreen "$(date '+ %b %d %H:%M')"
    echo

    colorspec='--white'
    [ "$ERRNO" != 0 ] && colorspec='--red'
    color "${colorspec}" '.. '
}
