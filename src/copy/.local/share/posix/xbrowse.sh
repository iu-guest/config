which tabbed >/dev/null || return
which surf   >/dev/null || return

# Sometimes I am forced to use graphical web-browser with
# JS/Cookies enabled. At least, I can keep them isolated.

__surf_url () (
    export DISPLAY=:0
    local url=$1
    HOME=${HOME}/.cache/surfurl/$(printf %s "${url}" | base32)
    tabbed -c -n "${url}" -r 2 surf -e "{}" -K "${url}"
)

__make_web_alias () {
    local url=$1
    local name="${2:-$1}"
    eval "alias %$name='__surf_url $url 2>/dev/null 1>/dev/null &'"
}

__make_web_alias github.com
__make_web_alias gitlab.com
__make_web_alias salsa.debian.org
__make_web_alias godaddy.com
__make_web_alias stackoverflow.com
__make_web_alias hub.docker.com
__make_web_alias couchsurfing.com
__make_web_alias online.sberbank.ru
__make_web_alias alfabank.ru/everyday/online/alfaclick/ alphabank.ru
__make_web_alias mentors.debian.net
__make_web_alias fpl.com
__make_web_alias rentcentral.net
__make_web_alias hh.ru
__make_web_alias jabber.at
unset -f __make_web_alias
