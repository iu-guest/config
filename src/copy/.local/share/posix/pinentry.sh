which gpg-connect-agent >/dev/null || return

# When using GPG auth key in ssh, gpg-agent must be informed about
# current terminal, otherwise it would spawn pinentry on wrong one,
# causing havoc.
__update_gpg_tty() {
    gpg-connect-agent updatestartuptty /bye >/dev/null
}
__make_wrapper() {
    local cmd="$1"
    eval "alias $cmd='__update_gpg_tty; command $cmd'"
}
__make_wrapper ssh
__make_wrapper git
unset -f __make_wrapper
