set rtp+=/usr/share/vim/addons
set nocompatible
syntax enable

filetype plugin indent on
packadd matchit
runtime! plugin/gzip.vim

set keymap=russian-jcukenwin
set iminsert=0
set imsearch=0
inoremap <C-\> <C-^>

set autoindent
set cmdwinheight=4
set cindent
set complete+=t "tag completion
set copyindent
set exrc
set foldclose=all
set foldlevel=99
set foldmethod=marker
set incsearch
set iskeyword=a-z,A-Z,_,.,39
set lisp
set list
set listchars=trail:-,tab:>.
set magic
set nohlsearch
set noloadplugins
set number
set ruler
set secure
set showmatch
set showcmd
set smartcase
set textwidth=72
set wildmenu
set wildmode=list:longest,list:longest
set infercase
set colorcolumn=80

" Vim thinks, that /bin/sh scripts must use backticks for command
" substitution. Make it believe that shell is actually bash.
let g:is_bash=1
let g:sh_fold_enabled=7

map Q gq
noremap j gj
noremap k gk
set tags+=codex.tags

au BufWrite *.hs :silent !git codex >/dev/null 2>&1 &
source ~/.vim/autogen/snippets.vim
