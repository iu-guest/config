setlocal expandtab
setlocal shiftwidth=4
setlocal foldmethod=syntax

if expand("%:t") ==# '.posixrc'
  setlocal path+=.local/share/posix
  setlocal suffixesadd+=.sh
endif
