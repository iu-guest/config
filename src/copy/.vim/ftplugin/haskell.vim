command! -bang -nargs=1 -complete=tag HSymbol call hsymbol#main(<f-args>, "<bang>")
command! -bang HSymbolWord call hsymbol#main(expand("<cword>"), "<bang>")
nnoremap <buffer> <leader>i :HSymbolWord<CR>
nnoremap <buffer> <leader>I :HSymbolWord!<CR>
