let s:enum = {ix, val -> string(ix + 1) . '. ' . val}
let s:cmd  = {sym -> 'hsymbol ' . sym . '< /tmp/hsymbol.dat'}
let s:not_found = {sym -> 'symbol `' . sym . "' not found" }

function! hsymbol#main(symbol, bang) abort
  let opts = systemlist(s:cmd(a:symbol))

  if len(opts) ==# 0
    echo s:not_found(a:symbol)
    return
  endif

  let choice = opts[0]
  if len(opts) ># 1
    let header = 'What module you want?'
    let choices = map(copy(opts), s:enum)
    let ix = inputlist(extend([header], choices)[0:&lines - 2])
    let choice = opts[ix - 1]
  endif

  let ex_cmd  = 'silent %!hsimport /dev/stdin -o /dev/stdout '
  let ex_cmd .= '-s "' . a:symbol . '" '
  let ex_cmd .= choice
  if a:bang ==# '!'
    let ex_cmd .= ' -a'
  endif
  execute(ex_cmd)
endfunction
