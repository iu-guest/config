m4_divert(-1)
m4_define(`rl_begin', `m4_esyscmd(`/usr/bin/printf %b "\x01"')')
m4_define(`rl_end', `m4_esyscmd(`/usr/bin/printf %b "\x02"')')
m4_define(`rl', `rl_begin`'$1`'rl_end')
m4_define(`color', `rl(m4_esyscmd(`tput -T linux setaf $1'))')
m4_define(`bold', `rl(m4_esyscmd(`tput -T linux bold'))')
m4_define(`MAKE_COLOR',`m4_dnl
export COLOR_$1="color($2)"
export COLOR_x$1="bold`'color($2)"
')
m4_define(`VIM_READ_ONLY',`# vim: ro')
m4_divert(0)

MAKE_COLOR(BLACK, 0)
MAKE_COLOR(RED, 1)
MAKE_COLOR(GREEN, 2)
MAKE_COLOR(YELLOW, 3)
MAKE_COLOR(BLUE, 4)
MAKE_COLOR(MAGENTA, 5)
MAKE_COLOR(CYAN, 6)
MAKE_COLOR(WHITE, 7)
export COLOR_RESET="rl(m4_esyscmd(`tput -T linux sgr0'))"
VIM_READ_ONLY
